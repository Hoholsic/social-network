import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";

let store = {
	_state: {
		profilePage: {
				posts: [
					{id: 1, massage: 'Hi, how are you?', data: '6:38 PM / Monday, October 22nd'},
					{id: 2, massage: 'Which country to visit next? This is a photo with my friends - celebrating in Bali .', data: '8:48 PM / Tuesday, October 23nd'},
					{id: 3, massage: 'It is my first post', data: '10:48 PM / Wednesday, October 24nd'}
				],
				newPostText: ''
		},
		massagePage: {
			dialogs: [
				{id: 1, name: 'Jeshua Stout'},
				{id: 2, name: 'Carmen Velasco'},
				{id: 3, name: 'Marie Jensen'},
				{id: 4, name: 'Leo Gill'},
				{id: 5, name: 'Nala Hester'},
				{id: 6, name: 'Orlando Diggs'},
				{id: 7, name: 'Aada Laine'}
			],
			messages: [
				{id: 1, message: 'Hi'},
				{id: 2, message: 'How are you?'},
				{id: 3, message: 'Im am fine'}
			],
			newMessagesText: ''
		}	
	},
	_callsubscribe(){},
	getState() {
		return this._state
	},
	subscribe(observer) {
		this._callsubscribe = observer;
	},
	dispatch(action) {

		this._state.profilePage = profileReducer(this._state.profilePage, action);
		this._state.massagePage = dialogsReducer(this._state.massagePage, action);

		this._callsubscribe(this._state);
	}
}

export default store;