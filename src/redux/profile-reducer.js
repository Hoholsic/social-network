import {profileAPI} from '../api/api';
import {stopSubmit} from "redux-form";

const ADD_POST = 'social-network/profilePage/ADD-POST';
const SET_USERS_PROFILE = 'social-network/profilePage/SET_USERS_PROFILE';
const SET_STATUS = 'social-network/profilePage/SET_STATUS';
const SAVE_PHOTO_SUCCESS = 'social-network/profilePage/SAVE_PHOTO_SUCCESS';

let initialState = {
	posts: [
		{id: 1, massage: 'Hi, how are you?', data: '6:38 PM / Monday, October 22nd'},
		{id: 2, massage: 'Which country to visit next? This is a photo with my friends - celebrating in Bali .', data: '8:48 PM / Tuesday, October 23nd'},
		{id: 3, massage: 'It is my first post', data: '10:48 PM / Wednesday, October 24nd'}
	],
	profile: null,
	status: ""
}

const profileReducer = (state = initialState, action) => {
	switch(action.type) {
		case ADD_POST:
			let newPost = {
				id: 4, 
				massage: action.newPostText, 
				data: '11:48 PM / Wednesday, October 24nd'
			};
			return{
				...state,
				posts: [...state.posts, newPost]
			};
		case SET_USERS_PROFILE: 
		return {
			...state,
			profile: action.profile
		}
		case SET_STATUS: 
		return {
			...state,
			status: action.status
		}
		case SAVE_PHOTO_SUCCESS: 
		return {
			...state,
			profile: {...state.profile, photos: action.photos}
		}
		default:{
			return state;
		}
	}  
}

export const addPost = (newPostText) => ({type: ADD_POST, newPostText});
export const setUserProfile = (profile) => ({type: SET_USERS_PROFILE, profile});
export const setStatus = (status) => ({type: SET_STATUS, status});
export const savePhotoSuccess = (photos) => ({type: SAVE_PHOTO_SUCCESS, photos});

export const getUserProfile = (userId) => async (dispatch) => {
	const response = await profileAPI.getProfile(userId);
	dispatch(setUserProfile(response.data));
}
export const getStatus = (userId) => async (dispatch) => {
	const response = await profileAPI.getStatus(userId);
	dispatch(setStatus(response.data));
}
export const updateStatus = (status) => async (dispatch) => {
	const response = await	profileAPI.updateStatus(status);
	if(response.data.resultCode === 0) {
		dispatch(setStatus(status));
	}
} 
export const savePhoto = (file) => async (dispatch) => {
	const response = await	profileAPI.savePhoto(file);
	if(response.data.resultCode === 0) {
		dispatch(savePhotoSuccess(response.data.data.photos));
	}
} 
export const saveProfile = (profile) => async (dispatch, getState) => {
	const userId = getState().auth.userId;
	const response = await	profileAPI.saveProfile(profile);
	if(response.data.resultCode === 0) {
		dispatch(getUserProfile(userId));
	} else {
		dispatch(stopSubmit("edit-profile", {_error: response.data.messages[0]}));
		return Promise.reject(response.data.messages[0]);
	}
} 

export default profileReducer