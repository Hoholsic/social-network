const SEND_MESSAGE = 'social-network/massagePage/SEND-MESSAGE';

let initialState = {
	dialogs: [
		{id: 1, name: 'Jeshua Stout'},
		{id: 2, name: 'Carmen Velasco'},
		{id: 3, name: 'Marie Jensen'},
		{id: 4, name: 'Leo Gill'},
		{id: 5, name: 'Nala Hester'},
		{id: 6, name: 'Orlando Diggs'},
		{id: 7, name: 'Aada Laine'}
	],
	messages: [
		{id: 1, message: 'Hi'},
		{id: 2, message: 'How are you?'},
		{id: 3, message: 'Im am fine'}
	]
}

const dialogsReducer = (state = initialState, action) => {
	switch(action.type) {
		case SEND_MESSAGE:
			let body = action.newMessagesBody;
			return {
				...state,
				messages: [...state.messages, {id: 4, message: body} ],

			};
		default:
			return state;
	}
}

export const sendMessage = (newMessagesBody) => ({type: SEND_MESSAGE, newMessagesBody});

export default dialogsReducer;