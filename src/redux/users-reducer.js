import {usersAPI} from '../api/api';
import {updataObjectInArray} from "../utils/object-helpers";

const FOLLOW = 'social-network/usersPage/FOLLOW';
const UNFOLLOW = 'social-network/usersPage/UNFOLLOW';
const SET_USERS = 'social-network/usersPage/SET-USERS';
const SET_CURRENT_PAGE = 'social-network/usersPage/SET-CURRENT-PAGE';
const SET_USERS_TOTAL_COUNT = 'social-network/usersPage/SET-USERS-TOTAL-COUNT';
const TOOGLE_IS_FETCHING = 'social-network/usersPage/TOOGLEsocial-network/auth/-IS-FETCHING';
const TOOGLE_IS_FOLLOWING_PROGRESS = 'social-network/usersPage/TOOGLE_IS_FOLLOWING_PROGRESS';

let initialState = {
	users: [],
	pageSize: 10,
	totalUsersCount: 0,
	currentPage: 1,
	isFetching: true,
	followingInProgress: []
}

const usersReducer = (state = initialState, action) => {
	switch(action.type) {
		case FOLLOW:
			return {
				...state,
				users: updataObjectInArray(state.users, action.userId, "id", {followed: true})
			};
		case UNFOLLOW:
			return {
				...state,
				users: updataObjectInArray(state.users, action.userId, "id", {followed: false})
			};
		case SET_USERS:
			return {
				...state,
				users: action.users
			};	
		case SET_CURRENT_PAGE:
			return {
				...state,
				currentPage: action.currentPage
			};
		case SET_USERS_TOTAL_COUNT: 
			return {
				...state,
				totalUsersCount: action.totalCount
			}
		case TOOGLE_IS_FETCHING:
			return {
				...state,
				isFetching: action.isFetching
			}
		case TOOGLE_IS_FOLLOWING_PROGRESS: 
			return {
				...state,
				followingInProgress: action.followingInProgress
					? [...state.followingInProgress, action.userId]
					: state.followingInProgress.filter(id => id != action.useid)
			}
		default:{
			return state;
		}
	}
}

export const followSuccess = (userId) => ({type: FOLLOW, userId});
export const unfollowSuccess = (userId) => ({type: UNFOLLOW, userId});
export const setUsers = (users) => ({type: SET_USERS, users});
export const setCurrentPage = (currentPage) => ({type: SET_CURRENT_PAGE, currentPage});
export const setTotalUsersCount = (totalCount) => ({type: SET_USERS_TOTAL_COUNT, totalCount});
export const toogleIsFetching = (isFetching) => ({type: TOOGLE_IS_FETCHING, isFetching});
export const toogleFollowingProgress= (followingInProgress, userId) => ({type: TOOGLE_IS_FOLLOWING_PROGRESS, followingInProgress, userId});

export const requestUsers = (currentPage, pageSize) => async (dispatch) => {
	dispatch(toogleIsFetching(true));
	dispatch(setCurrentPage(currentPage));
	let data = await usersAPI.getUsers(currentPage, pageSize);
	dispatch(toogleIsFetching(false));
	dispatch(setUsers(data.items));
	dispatch(setTotalUsersCount(data.totalCount));   
};
export const follow = (userId) => async (dispatch) => {
	dispatch(toogleFollowingProgress(true, userId));
	let response = await usersAPI.follow(userId);
	if(response.data.resultCode === 0) {
		dispatch(followSuccess(userId))
	}
	dispatch(toogleFollowingProgress(false, userId));   
};
export const unfollow = (userId) => async (dispatch) => {
	dispatch(toogleFollowingProgress(true, userId));
	let response = await usersAPI.unfollow(userId);
	if(response.data.resultCode === 0) {
		dispatch(unfollowSuccess(userId))
	}
	dispatch(toogleFollowingProgress(false, userId));   
}
export default usersReducer