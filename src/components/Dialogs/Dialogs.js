import React from 'react';
import classes from './Dialogs.module.css';
import Dialog from './Dialog/Dialog';
import Message from './Message/Message';
import {Redirect} from 'react-router-dom';
import {reduxForm, Field} from "redux-form";
import {required} from "../../utils/validators/validators";
import {Textarea} from "../Common/FormControls/FormControls";


const Dialogs = (props) => {	
	let state = props.massagePage;
	let dialogsElement = state.dialogs.map( d => <Dialog name={d.name} key={d.id} id={d.id}/>);
	let messagesElement = state.messages.map( m => <Message message={m.message} key={m.id}/>);

	let addNewMessage = (values) => {
		props.sendMessage(values.newMessagesBody);
	}
	return (
		<div className = {classes.dialogs}>
			<div className={classes.title}>
				Dialogs
			</div>
			<div className={classes.column}>
				<div className={classes.dialog}>
					{dialogsElement}
				</div>
				<div className={classes.messages}>
					<div>{messagesElement}</div>
					<AddMessageFormRedux onSubmit={addNewMessage}/>
				</div>
			</div>
		</div>
	);
}

const AddMessageForm = (props) => {
	return(
		<form onSubmit={props.handleSubmit}>
			<div>
				<Field placeholder="Enter your message" name="newMessagesBody" component={Textarea} validate={required} className={classes.text}/>
			</div>
			<div className={classes.btnblock}>
		   	<button className={classes.btn}>send</button>
		   </div>
		</form>
	)
}
const AddMessageFormRedux = reduxForm({form: 'dialogsAddMessageForm'}) (AddMessageForm);

export default Dialogs;