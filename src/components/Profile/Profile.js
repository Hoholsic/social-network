import React from 'react';
import classes from './Profile.module.css';
import MyPostsContainer from './MyPosts/MyPostsContainer';
import User from './User/User';

const Profile = (props) => {
	return (
		<div>
			<User isOwner={props.isOwner}
					profile={props.profile} 
					status={props.status} 
					updateStatus={props.updateStatus}
					savePhoto={props.savePhoto}
					saveProfile={props.saveProfile}/>
			<MyPostsContainer store={props.store}/>
		</div>
	);
}

export default Profile;