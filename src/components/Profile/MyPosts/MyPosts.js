import React from 'react';
import classes from './MyPosts.module.css'
import Post from './Post/Post'
import {reduxForm, Field} from "redux-form";
import {required} from "../../../utils/validators/validators";
import {Textarea} from "../../Common/FormControls/FormControls";

const MyPosts = React.memo(props => {
	let postsElements = props.posts.map( p => <Post key={p.id} massage={p.massage} data={p.data}/>);
	
	let addPost = (values) => {
		props.addPost(values.newPostText);
	}

	return (
		<div className={classes.post}>
			<MyPostsFormRedux onSubmit={addPost}/>
			<div className={classes.posts}>
				{postsElements}
			</div>
		</div>
	) 
})

const MyNewPostsForm = (props) => {
	return(
		<form onSubmit={props.handleSubmit}>
			<Field placeholder='Enter your post' name="newPostText" component={Textarea} className={classes.text} validate={required}/>
		   <div className={classes.btnblock}>
		   	<button className={classes.btn}>send</button>
		   </div>
		</form>
	)
}

const MyPostsFormRedux = reduxForm({form: 'profileMyNewPostsForm'}) (MyNewPostsForm);

export default MyPosts;