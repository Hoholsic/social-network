import React from 'react';
import classes from './User.module.css'
import Avatar from './Avatar/Avatar'
import Info from './Info/Info'

const User = (props) => {
	return (
		<div className={classes.user}>
			<Avatar savePhoto={props.savePhoto} profile={props.profile} isOwner={props.isOwner}/>
			<Info profile={props.profile} isOwner={props.isOwner} saveProfile={props.saveProfile} status={props.status} updateStatus={props.updateStatus}/>
		</div>
	);
}

export default User;