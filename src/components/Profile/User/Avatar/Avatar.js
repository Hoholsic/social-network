import React from 'react';
import classes from './Avatar.module.css';
import Preloader from '../../../Common/Preloader/Preloader';
import userPhoto from "../../../../assets/image/user.jpg";

const Avatar = (props) => {
	if(!props.profile) {
		return <Preloader/>
	}
	const onMainPhotoSelected = (e) => {
		if(e.target.files.length) {
			props.savePhoto(e.target.files[0])
		}
	}
	return (
		<div className = {classes.avatar}>
			<img src={props.profile.photos.large || userPhoto} alt="#"/>
			{props.isOwner && <input type="file" onChange={onMainPhotoSelected}/>}
		</div>
	);
}

export default Avatar;