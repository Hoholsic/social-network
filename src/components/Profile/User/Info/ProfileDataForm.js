import React from 'react';
import {reduxForm} from "redux-form";
import {createField, Input, Textarea} from "../../../Common/FormControls/FormControls.js";
import classes from "../../../Common/FormControls/FormControls.module.css";

const ProfileDataForm = ({ handleSubmit, profile, error}) => {
	return (
		<form onSubmit={handleSubmit}>
			<div>
				<div>
					<b>Full Name</b>: {createField("Full Name", "fullName", [], Input)}
				</div>
				<div>
					<b>Looking for a job</b>: {createField("","lookingForAJob", [], Input, {type: "checkbox"})}
				</div>
				<div>
					<b>My professional skills</b>: {createField("My professional skills", "lookingForAJobDescription", [], Textarea)}
				</div>
				<div>
					<b>About me</b>: {createField("About me", "aboutMe", [], Textarea)}
				</div>
			</div>
			<div>
				<div>
					<b>Contact</b>: {Object.keys(profile.contacts).map(key => {
						return <div key={key}>
							<b>{key}: {createField(key, "contacts." + key, [], Input)}</b>
						</div>
					})}
				</div>
			</div>
			<div> <button>save</button> </div>
			{error && <div className={classes.summaryError}>
				{error}
			</div>}
		</form>
	)
}
const ProfileDataFormReduxForm = reduxForm({form: 'edit-profile'}) (ProfileDataForm);
export default ProfileDataFormReduxForm;