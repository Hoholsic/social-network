import React, {useState, useEffect} from 'react';
import classes from './Info.module.css'
import Preloader from '../../../Common/Preloader/Preloader'
// import ProfileStatus from './ProfileStatus';
import ProfileStatusWithHooks from './ProfileStatusWithHooks';
import ProfileDataForm from "../Info/ProfileDataForm";

const Info = (props) => {
	let [editMode, setEditMode] = useState(false);

	if(!props.profile) {
		return <Preloader/>
	}
	const onSubmit =(formData) => {
		props.saveProfile(formData).then(() => {setEditMode(false)})
	}
	return (
		<div className={classes.info}>
			<ProfileStatusWithHooks  status={props.status} updateStatus={props.updateStatus}/>
			{editMode 
				? <ProfileDataForm initialValues={props.profile} profile={props.profile} onSubmit={onSubmit}/> 
				: <ProfileData profile={props.profile} isOwner={props.isOwner} goToEditMode={() => {setEditMode(true)}}/>}
		</div>	
	);
} 

const ProfileData = ({profile, isOwner, goToEditMode}) => {
	return (
		<div>
			<div className={classes.discription}>
				<div>
					<div className={classes.name}>
						{profile.fullName}
					</div>
					<div className={classes.job}>
						{profile.aboutMe}
					</div>
					<div className={classes.aboutJob}>
						{profile.lookingForAJobDescription}
					</div>
				</div>
				<div>
					<div className={classes.contacts}>
						<b>Contact</b>: {Object.keys(profile.contacts).map(key => {
							return <Contact key={key} contactTitle={key} contactValue={profile.contacts[key]}/>
						})}
					</div>
				</div>
			</div>
			{isOwner && <div className={classes.btn}> <button onClick={goToEditMode}>edit</button> </div>}
		</div>
	)
}
const Contact = ({contactTitle, contactValue}) => {
	return <div> <b>{contactTitle}</b>: {contactValue} </div>
}
export default Info;