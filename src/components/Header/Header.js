import React from 'react';
import classes from './Header.module.css'
import {NavLink} from 'react-router-dom';

const Header = (props) => {
	return <header className = {classes.header}>
 		<img className = {classes.logo} src="/img/logo.png" alt="#"/>
 		<div>
 			{ props.isAuth 
 				? <div>{props.login}  <button onClick={props.logout}>log out</button></div>
 				: <NavLink to={'/login'} className={classes.login} >login </NavLink>	
 			}
 		</div>
 	</header>
}

export default Header;