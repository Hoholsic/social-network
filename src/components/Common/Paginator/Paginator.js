import React, {useState} from 'react';
import classes from './Paginator.module.css';

const Paginator = ({totalItemsCount, pageSize, currentPage, onPageChenged, portionSize = 10}) => {
	let pagesCount = Math.ceil(totalItemsCount / pageSize);
	let pages = [];
	for (let i=1; i<=pagesCount; i++) {pages.push(i)}

	let portionCount = Math.ceil(pagesCount / portionSize);
	let [portionNumber, setPortionNumber] = useState(1)
	let leftPortionPegaNumber = (portionNumber - 1) * portionSize + 1;
	let rightPortionPegaNumber = portionNumber * portionSize;

	return(
		<div className={classes.pagination}>
			{portionNumber > 1 && <button onClick={() => {setPortionNumber(portionNumber - 1)}}>Prev</button>}
			{pages
				.filter(p => p >= leftPortionPegaNumber && p <= rightPortionPegaNumber)
				.map((p) => {
				return <div className={currentPage === p && classes.page}
				onClick={(e) => {onPageChenged(p);}}>{p}</div>})
			}
			{portionCount > portionNumber && <button onClick={() => {setPortionNumber(portionNumber + 1)}}>Next</button>}
		</div>
	)
}

export default Paginator;