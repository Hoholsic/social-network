import React from 'react';
import classes from './FormControls.module.css';
import {Field} from "redux-form";

const FormControls = ({input, meta, child, ...props}) => {
	const hasError = meta.touched && meta.error;
	return(
		<div className={hasError ? classes.borderError : " "}>
			{props.children}
			{hasError && <span className={classes.textError}>{meta.error}</span>}
		</div>
	)
}

export const Textarea = (props) => {
	const {input, meta, child, ...restProps} = props;
	return(
		<FormControls {...props}><textarea {...input} {...restProps}/></FormControls>
	)
}

export const Input = (props) => {
	const {input, meta, child, ...restProps} = props;
	return(
		<FormControls {...props}><input {...input} {...restProps}/></FormControls>
	)
}

export const createField = (placeholder, name, validators, component, props = {}, text = "") => (
	<div>
		<Field placeholder={placeholder} 
				name={name} 
				validate={validators} 
				component={component}
				{...props}/> {text}
	</div>
)