import React from 'react';
// import classes from './Login.module.css'
import {reduxForm, Field} from "redux-form";
import {Input, createField} from "../Common/FormControls/FormControls";
import {required} from "../../utils/validators/validators";
import {login} from '../../redux/auth-reducer';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import classes from "./../Common/FormControls/FormControls.module.css"; 

const LoginForm = ({handleSubmit, error, captchaUrl}) => {
	return (
		<form onSubmit={handleSubmit}>
			<div>
				<Field placeholder="Email" name="email" component={Input} validate={required}/>
			</div>
			<div>
				<Field placeholder="Password" name="password" type="password" component={Input} validate={required}/>
			</div>
			<div>
				<Field type="checkbox" name="rememberMe" component={Input}/> remember me
			</div>
			{captchaUrl && <img src={captchaUrl} alt="#"/>}
			{captchaUrl && createField("Symbols from image", "captcha", [required], Input)}
			{error && <div className={classes.summaryError}>
				{error}
			</div>}
			<div>
				<button type="submit">login</button>
			</div>
		</form>
	);
}

const LoginReduxForm = reduxForm({form: 'login'}) (LoginForm);

const Login = (props) => {
	const onSubmit = (formData) => {
		props.login(formData.email, formData.password, formData.rememberMe, formData.captcha)
	}
	if(props.isAuth) {
		return <Redirect to="/profile"/>
	}
	return (
		<div className = {classes.login}>
			<div className={classes.title}>Login</div>
			<LoginReduxForm onSubmit={onSubmit} captchaUrl={props.captchaUrl}/>
		</div>
	);
}
const mapStateToProps = (state) => ({
	captchaUrl: state.auth.captchaUrl,
	isAuth: state.auth.isAuth
})
export default connect(mapStateToProps, {login})(Login);