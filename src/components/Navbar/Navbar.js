import React from 'react';
import {NavLink} from 'react-router-dom';
import classes from './Navbar.module.css'

const Navbar = () => {
	return (
		<nav className = {classes.navbar}>
	 		<div>
	 			<NavLink className={classes.item} activeClassName={classes.active} to="/profile">Profile</NavLink>
	 		</div>
	 		<div>
	 			<NavLink className={classes.item} activeClassName={classes.active} to="/dialogs">Messages</NavLink>
	 		</div>
	 		<div>
	 			<NavLink className={classes.item} activeClassName={classes.active} to="/news">News</NavLink>
	 		</div>
	 		<div>
	 			<NavLink className={classes.item} activeClassName={classes.active} to="/music">Music</NavLink>
	 		</div>
	 		<div>
	 			<NavLink className={classes.item} activeClassName={classes.active} to="/settings">Settings</NavLink>
	 		</div>
	 		<div>
	 			<NavLink className={classes.item} activeClassName={classes.active} to="/users">Users</NavLink>
	 		</div>
	 	</nav>
 	);
}

export default Navbar;