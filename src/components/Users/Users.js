import React from 'react';
import classes from './Users.module.css';
import User from "./User";
import Paginator from "../Common/Paginator/Paginator";

const Users = ({currentPage, totalItemsCount, pageSize, onPageChenged, users, ...props}) => {
	return (
		<div className={classes.users}>
			<Paginator currentPage={currentPage} totalItemsCount={totalItemsCount} pageSize={pageSize} onPageChenged={onPageChenged} />
			{
				users.map( u => 
					<User key={u.id} followingInProgress={props.followingInProgress} user={u} unfollow={props.unfollow} follow={props.follow} />)
			}
		</div>
	)
}

export default Users;
