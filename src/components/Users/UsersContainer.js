import React from 'react';
import {follow, unfollow, setCurrentPage, toogleFollowingProgress, requestUsers} from '../../redux/users-reducer';
import {connect} from 'react-redux';
import {compose} from 'redux';
import Users from './Users';
import Preloader from '../Common/Preloader/Preloader';
import {getUsers, getPageSize, getTotalUsersCount, getCurrentPage, getIsFetching, getFollowingInProgress} from "../../redux/users-selectors.js";


class UsersContainer extends React.Component {
	componentDidMount() {
		const {currentPage, pageSize} = this.props;
		this.props.requestUsers(currentPage, pageSize);
	}
	onPageChenged = (pageNumber) => {
		const {pageSize} = this.props;
		this.props.requestUsers(pageNumber, pageSize);
	}
	render() {
		return (
			<>
				{this.props.isFetching ? <Preloader /> : null}
				<Users totalItemsCount={this.props.totalItemsCount}
						 pageSize={this.props.pageSize}
						 currentPage={this.props.currentPage}
						 onPageChenged={this.onPageChenged}
						 users={this.props.users}
						 follow={this.props.follow}
						 unfollow={this.props.unfollow}
						 followingInProgress={this.props.followingInProgress}
				/>
			</>
		);
	}
}
let mapStateToProps = (state) => {
	return {
		users: getUsers(state),
		pageSize: getPageSize(state),
		totalItemsCount: getTotalUsersCount(state),
		currentPage: getCurrentPage(state),
		isFetching: getIsFetching(state),
		followingInProgress: getFollowingInProgress(state)
	}
}

export default compose(connect(mapStateToProps,
	{follow, unfollow, setCurrentPage, toogleFollowingProgress, requestUsers}))(UsersContainer);

