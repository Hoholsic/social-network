import React from 'react';
import classes from './Users.module.css';
import userPhoto from "../../assets/image/user.jpg";
import {NavLink} from 'react-router-dom';

const User = ({user, followingInProgress, unfollow, follow}) => {
	return (
		<div className = {classes.user}>
			<div className = {classes.avatar}>
			<NavLink to={'/profile/' + user.id}>
				<img src={user.photos.small != null ? user.photos.small : userPhoto } alt="#" />	
			</NavLink>
			</div>
			<div className = {classes.description}>
				<div className = {classes.fullname}>{user.name}</div>
				<div className = {classes.status}>{user.status}</div>
				<div className = {classes.location}>
					<div className = {classes.country}>{"user.location.country"}</div>
					<div className = {classes.city}>{"user.location.city"}</div>
				</div>
			</div>
			<div className = {classes.btn}>
				{user.followed 
					? <button disabled={followingInProgress.some(id => id === user.id)} onClick = {() => {
						unfollow(user.id)}}>unfollow</button>
		         : <button disabled={followingInProgress.some(id => id === user.id)} onClick = {() => {
			         follow(user.id)}}>follow</button>
				}
			</div>
		</div>
	)
}

export default User;
