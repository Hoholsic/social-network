import React, {Suspense} from 'react';
import './App.css';
import {Route, withRouter, Switch, Redirect} from 'react-router-dom';
import HeaderContainer from './components/Header/HeaderContainer';
import Navbar from './components/Navbar/Navbar';
import UsersContainer from './components/Users/UsersContainer';
import Music from './components/Music/Music';
import News from './components/News/News';
import Settings from './components/Settings/Settings';
import Login from './components/Login/Login';
import {initializeApp} from './redux/app-reducer';
import {connect} from 'react-redux';
import {compose} from 'redux';
import Preloader from './components/Common/Preloader/Preloader';
import {withSuspense} from './hoc/withSuspense';

const ProfileContainer = React.lazy(() => import('./components/Profile/ProfileContainer'));
const DialogsContainer = React.lazy(() => import('./components/Dialogs/DialogsContainer'));

class App extends React.Component {
  componentDidMount() {
    this.props.initializeApp();
  }
  render() {
    if(!this.props.initialized){
      return <Preloader />
    }
    return (
      <div className="wrapper">
      	<HeaderContainer/> 
      	<div className = "inner">
        	<Navbar/>
        	<div className = "content">
            <Switch>
              <Route exact path='/' render = { () => <Redirect to = '/profile' />}/>
              <Route path='/profile/:userId?' render = {withSuspense(ProfileContainer)}/>
              <Route path='/dialogs' render = {withSuspense(DialogsContainer)}/> 
              <Route path='/music' render = { () => <Music/> }/>
              <Route path='/news' render = { () => <News/> }/>
              <Route path='/settings' render = { () => <Settings/> }/>
              <Route path='/users' render = { () => <UsersContainer/> }/>
              <Route path='/login' render = { () => <Login/> }/>
            </Switch>
        	</div>
      	</div>
      </div>   
    );  
  }
}

const mapStateToProps = (state) => ({
  initialized: state.app.initialized
})

export default compose(withRouter, connect(mapStateToProps, {initializeApp}))(App);